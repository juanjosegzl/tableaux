import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from parser import prepare

class Handler:
    def __init__(self, txtResult, txtFormula):
        self.premises = {
        }

        self.variables = {
            'Ale': 'P',
            'Weizen': 'W',
            'Lager': 'L',
            'Airlock adecuado': '~A',
            'Airlock Flojo': 'A',
            'Fermentación muy caliente': 'Χ',
            'Fermentación muy fría': 'Ω',
            'Ferment. temp. correcta': 'Ψ',
            'Terminó fermentación': 'D',
            'No Terminó fermentación': '~D',
            'Olor a huevo': 'H',
            'No esta fermentando': 'N',
            'Airlock obstruido': 'α',
            'Sabor a sidra': 'γ',
            'Mucha carbonatación': 'β',
            'Sabor afrutado': 'ε',
            'Muy turbia': 'ᚠ',
            'El cuerpo es muy ligero': 'ᚦ',
            'Hay bacterias': 'B',
            'La fermentación está muy fría.': 'F',
            'La fermentación está muy caliente.': 'C',
            'La fermentación fue muy vigorosa.': 'V',
            'Es un aspecto normal de la producción.': 'O',
            'Se añadió mucha azúcar al embotellar.': 'Z',
            'Adicionalmente puede tener un sabor afrutado': 'ε',
            'El molido fue muy fino': 'ᚢ',
            'El molido fue muy grueso': 'ᚩ',
            'La temperatura de la fermentación fue muy baja': 'Φ',
            'La temperatura de la fermentación fue muy alta': 'Υ',
        }

        self.causes = {
            'B': 'Hay bacterias',
            'F': 'La fermentación está muy fría.',
            'C': 'La fermentación está muy caliente.',
            'V': 'La fermentación fue muy vigorosa.',
            'O': 'Es un aspecto normal de la producción.',
            'Z': 'Se añadió mucha azúcar al embotellar.',
            'ε': 'Adicionalmente puede tener un sabor afrutado',
            'ᚢ': 'El molido fue muy fino',
            'ᚩ': 'El molido fue muy grueso',
            'Φ': 'La temperatura de la fermentación fue muy baja',
            'Υ': 'La temperatura de la fermentación fue muy alta',
        }

        self.txtResult = txtResult
        self.txtFormula = txtFormula

    def call(self):
        detected_causes = prepare(self.premises.values())
        text = ""
        for detected_cause in detected_causes:
            if detected_cause == self.premises['problem']:
                continue
            text += self.causes[detected_cause] + '\n'

        self.txtResult.set_text(text)

    def onDestroy(self, *args):
        Gtk.main_quit()

    def onChangeTypeBeer(self, combo):
        self.premises['type'] = {
            'Ale': 'P',
            'Weizen': 'W',
            'Lager': 'L',
        }[combo.get_active_text()]
        self.call()

    def onChangeAirlock(self, combo):
        self.premises['airlock'] = {
            'Adecuado': '~A',
            'Flojo': 'A',
        }[combo.get_active_text()]
        self.call()

    def insertVariable(self, combo):
        t = self.variables[combo.get_active_text()]
        self.txtFormula.set_text(self.txtFormula.get_text() + f' {t} ')

    def addCausa(self, combo):
        t = self.variables[combo.get_active_text()]
        self.txtFormula.set_text(self.txtFormula.get_text() + f' {t} ')

    def addAnd(self, _):
        self.txtFormula.set_text(self.txtFormula.get_text() + ' ^ ')

    def addOr(self, _):
        self.txtFormula.set_text(self.txtFormula.get_text() + ' v ')

    def addThen(self, _):
        self.txtFormula.set_text(self.txtFormula.get_text() + ' > ')

    def addNo(self, _):
        self.txtFormula.set_text(self.txtFormula.get_text() + ' ~ ')

    def addFormula(self, _):
        f = self.txtFormula.get_text()
        with open('even_more_premises', 'a') as file_:
            file_.write(f'{f}\n')
        self.txtFormula.set_text('')

    def onChangeTemperature(self, entry):
        print(entry.get_text())
        t = int(entry.get_text())
        t = 'c' if t > 22 else ('f' if t < 16 else None)
        if t is None:
            del self.premises['temperature']
        self.premises['temperature'] = {
            'c': 'C',
            'f': 'F',
        }[t]
        self.call()

    def onChangeFermentationTemperature(self, entry):
        t = int(entry.get_text())
        c = 0
        if t < 17:
            c = 0
        elif t < 23:
            c = 1
        else:
            c = 2
        self.premises['fermentation_temperature'] = {
            0: 'Ω',
            1: 'Ψ',
            2: 'Χ',
        }[c]
        self.call()

    def onChangeDaysFermentation(self, entry):
        self.premises['days'] = {
            False: '~D',
            True: 'D',
        }[int(entry.get_text()) > 15]
        self.call()

    def onChangeProblem(self, combo):
        self.premises['problem'] = {
            'Olor a huevo': 'H',
            'No esta fermentando': 'N',
            'Airlock obstruido': 'α',
            'Sabor a sidra': 'γ',
            'Mucha carbonatación': 'β',
            'Sabor afrutado': 'ε',
            'Muy turbia': 'ᚠ',
            'El cuerpo es muy ligero': 'ᚦ',
        }[combo.get_active_text()]
        self.call()

builder = Gtk.Builder()
builder.add_from_file("window2.glade")
txtResult = builder.get_object("txtResult")
txtFormula = builder.get_object("txtFormula")
handler = Handler(txtResult, txtFormula)
builder.connect_signals(handler)

window = builder.get_object("window1")
window.set_title("Problemas en la fermentación")
window.show_all()

Gtk.main()
