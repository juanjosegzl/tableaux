from dataclasses import dataclass

class Formula:
    def negate(self):
        return NegatedFormula(self)

    def normalize(self):
        if isinstance(self, AtomicFormula):
            return self

        # |A^B| = |A|^|B|
        # |AvB| = |A|v|B|
        and_or_normalization = lambda f: BinaryFormula(
            f.operator,
            f.sub1.normalize(),
            f.sub2.normalize()
        )

        def negation_normalization(f):
            operator = f.sub.operator
            if not operator:
                return f
            if operator == "v" or operator == "^":
                # |~(A&B)| = |~A|v|~B|
                # |~(AvB)| = |~A|&|~B|
                sub1 = f.sub.sub1.negate().normalize()
                sub2 = f.sub.sub2.negate().normalize()
                op = 'v' if operator == '^' else '^'
                return BinaryFormula(op, sub1, sub2)
            elif operator == ">":
                # |~(A->B)| = |A|&|~B|
                sub1 = f.sub.sub1.normalize();
                sub2 = f.sub.sub2.negate().normalize();
                return BinaryFormula('^', sub1, sub2);
            elif operator == "<>":
                # |~(A<->B)| = |A&~B|v|~A&B|
                sub1 = BinaryFormula('^', f.sub.sub1, f.sub.sub2.negate()).normalize();
                sub2 = BinaryFormula('^', f.sub.sub1.negate(), f.sub.sub2).normalize();
                return BinaryFormula('v', sub1, sub2);
            elif operator == "~":
                # |~~A| = |A|
                return f.sub.sub.normalize()

        normalization = {
            '^': and_or_normalization,
            'v': and_or_normalization,
            # |A->B| = |~A|v|B|
            '>': lambda f: BinaryFormula(
                'v',
                f.sub1.negate().normalize(),
                f.sub2.normalize()
            ),
            # |A<->B| = |A&B|v|~A&~B|
            '<>':  lambda f: BinaryFormula(
                'v',
                BinaryFormula('^', f.sub1, f.sub2).normalize(),
                BinaryFormula('^', f.sub1.negate(), f.sub2.negate()).normalize(),
            ),
            '~': negation_normalization,
        }
        return normalization[self.operator](self)

    def __eq__(self, other):
        print(str(self), str(other))
        return str(self) == str(other)

@dataclass
class AtomicFormula(Formula):
    predicate: str
    type_: str = 'literal'
    operator: str = ''

    def __str__(self):
        return f'{self.predicate}'


@dataclass
class BinaryFormula(Formula):
    def __init__(self, operator, sub1, sub2):
        self.operator: str = operator
        self.sub1: AtomicFormula = sub1
        self.sub2: AtomicFormula = sub2
        self.type_: str = 'alpha' if self.operator == '^' else 'beta'

    def __str__(self):
        return f'<{self.sub1}{self.operator}{self.sub2}>'

    def __repr__(self):
        return str(self)

@dataclass
class NegatedFormula(Formula):
    def __init__(self, sub):
        self.sub: str = sub
        self.operator: str = '~'
        self.type_: str = NegatedFormula._get_type(sub)

    @staticmethod
    def _get_type(sub):
        if sub.operator == '~':
            return 'doublenegation'
        return {
            'literal': 'literal',
            'alpha': 'alpha',
            'beta': 'beta' if sub.operator == '<>' else 'alpha'
        }[sub.type_]

    def __str__(self):
        return f'<{self.operator}{self.sub}>'

    def __repr__(self):
        return str(self)
