from copy import deepcopy, copy


class Prover:
    def __init__(self, init_formulas, parser):
        self.init_formulas = init_formulas
        self.init_formulas_normalized = [f.normalize()
                                         for f in init_formulas]
        self.steps = 0
        self.depth_limt = 2
        self.parser = parser
        self.tree = Tree(self)

    priorities = {
        'alpha': 1,
        'beta': 2
    }

    # Rules for tableau
    @staticmethod
    def alpha(branch, nodes):
        #   a
        # ------
        #   a1
        #   a2

        node = nodes[0]
        subnode1 = Node(node.formula.sub1, Prover.alpha, nodes)
        subnode2 = Node(node.formula.sub2, Prover.alpha, nodes)

        branch.add_node(subnode1)
        branch.add_node(subnode2)

        branch.try_close(subnode1)
        if not branch.closed:
            branch.try_close(subnode2)

    @staticmethod
    def beta(branch, nodes):
        #     b
        # ---------
        #  b1 | b2

        branch.tree.open_branches.insert(0, branch.copy())

        node = nodes[0]
        subnode1 = Node(node.formula.sub1, Prover.beta, nodes)
        subnode2 = Node(node.formula.sub2, Prover.beta, nodes)

        branch.add_node(subnode1)
        branch.tree.open_branches[0].add_node(subnode2)

        branch.try_close(subnode1)
        branch.tree.open_branches[0].try_close(subnode2)


    def next_step(self):
        self.steps += 1

        try:
            todo = self.tree.open_branches[0].todo_list.pop(0)
        except IndexError:
            todo = None
        if todo:
            next_rule = todo[0]
            args = todo[1:]
            next_rule(self.tree.open_branches[0], args)

            if not self.tree.open_branches:
                # finished
                return True

            return self.next_step()

class Node:
    counter = 0

    def __init__(self, formula, from_rule=None, from_nodes=None):
        self.formula = formula
        self.from_rule = from_rule if from_rule is not None else []
        self.from_nodes = from_nodes if from_nodes is not None else []
        self.type_ = formula.type_
        Node.counter += 1
        self.id_ = Node.counter
        self.used = False

    def get_expansion_rule(self):
        return getattr(Prover, self.type_), Prover.priorities[self.type_]

    def __repr__(self):
        return str(self.formula)

    def __eq__(self, other_formula):
        return str(self.formula) == str(other_formula)

class Tree:
    def __init__(self, prover):
        self.prover = prover
        self.parser = prover.parser
        self.num_nodes = 0
        init_branch = Branch(self)
        for init_formula in prover.init_formulas_normalized:
            init_branch.add_node(Node(init_formula))

        self.open_branches = [init_branch]
        self.closed_branches = []

    def close_branch(self, branch, complementary1, complementary2):
        branch.closed = True
        self.mark_used_nodes(branch, complementary1, complementary2)

        self.open_branches.remove(branch)
        self.closed_branches.append(branch)

    def mark_used_nodes(self, branch, complementary1, complementary2):
        # todo Q
        ancestors = [complementary1, complementary2]
        while ancestors:
            n = ancestors.pop(0)
            if n.used:
                continue
            ancestors.extend(n.from_nodes)
            n.used = True


class Branch:
    counter = 0

    def __init__(self, tree, nodes=None, todo_list=None, closed=False):
        self.tree = tree
        self.nodes = nodes if nodes is not None else []
        self.literals = []
        # [[Prover.alpha, node]]
        # {prover:, node}
        self.todo_list = todo_list if todo_list else []
        self.closed = closed
        Branch.counter += 1
        self.id_ = Branch.counter

    def copy(self):
        return Branch(
            self.tree,
            copy(self.nodes),
            deepcopy(self.todo_list),
            self.closed
        )

    def try_close(self, node):
        negated_formula = (node.formula.sub
                           if node.formula.operator == '~'
                           else node.formula.negate())
        for self_node in self.nodes:
            if(self_node == negated_formula):
                self.tree.close_branch(self, node, self_node)
                return True
        return False

    def add_node(self, node):
        add_to_todo = not self.contains_formula(node.formula)
        self.nodes.append(node)
        self.tree.num_nodes += 1
        if node.type_ == 'literal':
            self.literals.append('node')
        if not self.closed and add_to_todo:
            self.expand_todo_list(node)
#        node.expansion_step = self.tree.prover.step
        return node

    def contains_formula(self, formula):
        for node in self.nodes:
            if str(node.formula) == str(formula):
                return True
        return False

    def expand_todo_list(self, node):
        if node.type_ == 'literal':
            return None

        expansion_rule, priority = node.get_expansion_rule()
        c = 0
        for i, todo in enumerate(self.todo_list):
            c = i
            _, todo_node_priority = todo[1].get_expansion_rule()
            if priority <= todo_node_priority:
                break
        self.todo_list.insert(c, [expansion_rule, node])

    def __repr__(self):
        return '<' + ','.join([str(n.formula) for n in self.nodes]) + '>'
