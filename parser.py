import re
import sys
import traceback

from formula import AtomicFormula, BinaryFormula, NegatedFormula
from prover import Prover


class Parser:
    @staticmethod
    def parse_input(input_):
        premises, conclusion = input_.split('|=')

        conclusion = Parser.parse_formula(conclusion)
        premises = [Parser.parse_formula(f) for f in premises.split(',')]

        return premises, conclusion

    @staticmethod
    def _is_enclosed(formula):
        return (formula.count("(") == 1
            and formula.count(")") == 1
            and formula[0] == "("
            and formula[-1] == ")")

    @staticmethod
    def parse_formula(formula):
        formula = Parser._tidy_formula(formula)
        if not formula:
            return False

        match = (
            re.search(r'<>', formula)
            or re.search(r'>', formula)
            or re.search(r'v', formula)
            or re.search(r'\^', formula)
        )
        if match:
            # formula has a connective
            new_formula, subformulas = Parser.hide_subformulas(formula)

            # check iuf connective was inside parens
            match = (
                re.search(r'<>', new_formula)
                or re.search(r'>', new_formula)
                or re.search(r'v', new_formula)
                or re.search(r'\^', new_formula)
            )
            if match:
                # connective is the main operator
                connective = match.group(0)
                new_formula = new_formula.replace(connective, "%split", 1);
                # restore subformulas
                for i, subformula in enumerate(subformulas):
                    new_formula = new_formula.replace(f"%{i}", subformula)
                sub1, sub2 = new_formula.split('%split')
                return BinaryFormula(
                    connective,
                    Parser.parse_formula(sub1),
                    Parser.parse_formula(sub2)
                )

        if '~' in formula:
            return NegatedFormula(Parser.parse_formula(formula[1:]))

        return AtomicFormula(formula)

    @staticmethod
    def _tidy_formula(formula):
        tidy = re.sub(r'\s', '', formula).replace('[', '(').replace(']', ')')

        if not Parser._are_parentheses_balanced(tidy):
            return False

        if Parser._is_enclosed(tidy):
            return tidy[1:-1]

        return tidy

    @staticmethod
    def _are_parentheses_balanced(formula):
        pars = 0
        for c in formula:
            if c == '(':
                pars += 1
            elif c == ')':
                if pars == 0:
                    return False
                pars -= 1
        return pars == 0

    @staticmethod
    def hide_subformulas(formula):
        subformulas = []
        parentheses_depth = 0
        new_formula = ""
        storing_at_index = -1
        for c in formula:
            if c == '(':
                parentheses_depth += 1
                if parentheses_depth == 1:
                    storing_at_index = len(subformulas)
                    try:
                        subformulas[storing_at_index] = ""
                    except IndexError:
                        subformulas.append("")
                    new_formula += "%" + str(storing_at_index)
            if storing_at_index == -1:
                new_formula += c
            else:
                subformulas[storing_at_index] += c
            if c == ")":
                parentheses_depth -= 1
                if parentheses_depth == 0:
                    storing_at_index = -1
        return new_formula, subformulas

def tests():
    inputs = (
        '(r^p) > q, p, r |= q',
        'a<>(p^q^r), p, q, r |= a',
        'qvr>p, q |= p',
        'p^q, p, q |= q',
        '~p, q |= p^q',
        'pvq, ~p |= ~p^q',
        'p^q, ~p |= pvq',
        'avb, b |= b',
        'a>b, a |= b',
        'avb, a, b |= b',
        'a>b, a |= b',
        'a>b, b>c, c>d, d>f, f>g, a |= g',
        '(avb)>d, a |= d',
        '(~L^H)>B, P<>(~L),       P, H |= B',
        '(~L^H)>B, P<>(~L),       L, H |= B',
        '(N^~D^~A > F), (N^~D^~A > C), N, ~D, ~A |= F',
        '(N^~D^~A > F), (N^~D^~A > C), N, ~D, ~A |= C',
        '(N^~D^~A > C), ~A<>E, ~D<>J, N, J, ~A |= C',
        'V > ~C^~F, α > V, α, α<>~A |= V',
    )

    for i in inputs:
        prove(i)

def prove(i):
    parser = Parser()
    premises, conclusion = parser.parse_input(i)
    init_formulas = premises[:]
    init_formulas.append(conclusion.negate())

    prover = Prover(init_formulas, parser)
    prover.next_step()
    print('{}. Does{} entail. {} closed branch{}'.format(
        i,
        ' not' if prover.tree.open_branches else '',
        len(prover.tree.closed_branches),
        'es' if len(prover.tree.closed_branches) > 1 else ''
    ))
    return not prover.tree.open_branches

def prepare(more_premises):
    premises = [
        '(~L^H)>B',  # No es lager y huele a huevo, son bacterias
        '(L^H)>O',  # Si es lager y huele a huevo puede que esté bien
        'P>(~L)',  # Si es Pale no es Lager
        '(N^~D^~A)>F',  # No está fermentando and no es vieja y el airlock no esta flojo entonces, esta muy frio
        '(N^~D^~A)>C',  # No está fermentando and no es vieja y el airlock no esta flojo entonces, esta muy caliente
        'α > V', # si el airlock esta sucio esntonces fermentacion vigorosa
        'P > (~W)',
        'L > (~W)',
        'β ^W > O',
        '(β ^~W)vγ>Z',  # nucha carbonatacion o γ sabor a sidra, mucha azucar
        'ε > C',  # si muy caliente entonces sabro afrutado
        'C > ε',
        '~D <> J',  # Suimnonimo, no vieja igual a muy joven
        'ᚠ ^ D ^ (L v P) > ᚢ',  # si esta turbia el molido fue muy fino
        'ᚠ ^ J > O',  # si esta turbia pero sigue fermentando es normasl
        'ᚠ ^ W > O',  # si esta turbia el molido fue muy fino
        'ᚦ ^ (P v W) > ᚩ',  # si el cuerpo esta muy ligero y no es lagger entonces el molido fue muy grueso
        'ᚦ ^ Ψ ^ L > O',  # si el cuerpo esta muy ligero y es lagger entonces normal
        'ᚦ ^ Ω ^ L > Φ',  # si el cuerpo esta muy ligero y es lagger fermentacion fria
        'ᚦ ^ Χ ^ L > Υ',  # si el cuerpo esta muy ligero y es lagger fermentacion caliente
    ]
    with open('even_more_premises') as f:
        content = f.readlines()
    content = [x.strip() for x in content]

    premises.extend(content)

    causes = (
        'B',  # Bacterias
        'F',  # muy frio
        'C',  # muy caliente
        'V',  # fermentación vigorosa
        'O',  # aspecto normal
        'Z',  # Mucha azúcar
        'ε',
        'ᚢ',  # el molido fue muy fino
        'ᚩ',  # El molido fue muy grueso
        'Φ',  # Lafermentacion fue muy fria
        'Υ',  # fermentacion muy caliente
    )

    applicable_premises = set()
    for premise in premises:
        for more_premise in more_premises:
            if more_premise in premise:
                applicable_premises.add(premise)

    applicable_premises = list(applicable_premises)
    applicable_premises.extend(more_premises)

    premises_input = ', '.join(applicable_premises)

    detected_causes = []
    for cause in causes:
        input_ = f'{premises_input} |= {cause}'
        try:
            if prove(input_):
                detected_causes.append(cause)
                premises_input += f', {cause}'
        except Exception:
            pass
    return detected_causes

if __name__ == '__main__':
    prepare(sys.argv[1:])
